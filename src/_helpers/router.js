import Vue from 'vue';
import Router from 'vue-router';

import HomePage from '../homepage/HomePage'
import Dashboard from '../dashboard/Dashboard'
import LoginPage from '../login/LoginPage'
import RegisterPage from '../register/RegisterPage'
import ProfilePage from '../profile/ProfilePage'

Vue.use(Router);
const username = "/abhimanyu"
export const router = new Router({
  mode: 'history',
  routes: [
    { path: '/about', component: HomePage },
    { path: '/', component: Dashboard},
    { path: '/login', component: LoginPage },
    { path: '/register', component: RegisterPage },
    { path: '/:username' , component: ProfilePage,  props :true},
    // otherwise redirect to home
    { path: '*', redirect: '/' }
  ]
});

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const porivatePages = ['/'];
  const authRequired = porivatePages.includes(to.path);
  console.log(to.path, authRequired)
  const loggedIn = localStorage.getItem('user');

  if (authRequired && !loggedIn) {
    return next('/login');
  }

  next();
})